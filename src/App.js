import React, { useState } from 'react';
import {
  Route, Routes, useNavigate
} from 'react-router-dom';
import './App.css';
import { UserProvider } from './context/UserContext';
import Home from './page/Home';
import Login from './page/Login';
import Register from './page/Register';


function App() {
  const [authData, setAuthData] = useState(JSON.parse(localStorage.getItem("authenticationData") || "{}"));
  const navigate = useNavigate();
  
  const setAuthenticationData = (data) => {
    setAuthData(data);
    if(!!data){
      localStorage.setItem("authenticationData", JSON.stringify(data));
      navigate("/home");  
    } else {
      localStorage.clear();
      navigate("/login");
    }
    
  };

  return (
    <div className="App" >
      <UserProvider value={{
        authData,
        setAuthenticationData
      }}>
        
          <Routes>
            <Route exact path='/' element={< Login />}></Route>
            <Route exact path='/login' element={< Login />}></Route>
            <Route exact path='/home' element={< Home />}></Route>
            <Route exact path='/register' element={< Register />}></Route>
          </Routes>
      </UserProvider>

    </div>
  );
}



export default App;
