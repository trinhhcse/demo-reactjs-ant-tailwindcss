import { useUser } from '../context/UserContext';


function Home() {
    const { authData, setAuthenticationData } = useUser();

    const logout = () => {
        setAuthenticationData(null);
    }

    return (
        <div className="App" >

            <p>HomePage</p>
            <p>{JSON.stringify(authData)}</p>
            <button onClick={logout}>Logout</button>
        </div>
    );
}

export default Home;
