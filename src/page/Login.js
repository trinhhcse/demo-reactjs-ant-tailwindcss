
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input } from 'antd';
import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import user from '../api/user';
import { useUser } from '../context/UserContext';


const Login = () => {
    const { authData, setAuthenticationData } = useUser();
    const navigate = useNavigate();
    const [form] = Form.useForm();
    useEffect(() => {
        let { username, password } = authData || {};
        console.log(authData);
        if(username && password) {
            form.setFieldsValue({
                username,
                password,
                remember:true
            })
            
        }
      return () => {
        
      }
    }, [])

    const onFinish = async (values) => {
        try {
            
            let { username, password, remember } = values;
            let result = await user.login(username, password);
            
            if (!!result) {
                remember && setAuthenticationData(result);
                navigate.navigate("/home");
            } else {
                //TODO: Handle error
            }
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <Form
            form={form}
            name="normal_login"
            className="w-64 ml-auto mr-auto mt-10"
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
        >
            <h1 className="mb-4 font-bold text-lg">Đăng nhập </h1>
            <Form.Item
                name="username"
                rules={[
                    {
                        required: true,
                        message: 'Please input your Username!',
                    },
                ]}
            >
                <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
            </Form.Item>
            <Form.Item
                name="password"
                rules={[
                    {
                        required: true,
                        message: 'Please input your Password!',
                    },
                ]}
            >
                <Input
                    prefix={<LockOutlined className="site-form-item-icon" />}
                    type="password"
                    placeholder="Password"
                />
            </Form.Item>

            <Form.Item>
                <Form.Item name="remember" valuePropName="checked" noStyle>
                    <Checkbox>Remember me</Checkbox>
                </Form.Item>

            </Form.Item>

            <Form.Item>
                <Button type="primary" htmlType="submit" className="login-form-button bg-blue-600	">
                    Đăng nhập
                </Button>
                <a className="px-2" href="/register">Đăng ký</a>
            </Form.Item>
        </Form>
    );
};

export default Login;