import axios from 'axios';

//TODO: Get base url from ENV
const api = axios.create({
  baseURL: "http://localhost:8088", 
  timeout: 30000,
});

api.interceptors.response.use(
  (response) => {
    if (response.data) {
      return response.data;
    }
    return response;
  },
  (error) => {
    return Promise.reject(error);
  },
);

export default api;
