import api from "./api"

const login = async (username,password) => {
    return api.post("/login",{username,password});
}

const register = async (username,password) => {
    return api.post("/register",{username,password});
}

const getAllUser = async () => {
    return api.get("/getAllUser");
}

const deleteUser = async (id) => {
    return api.get(`/delete/${id}`);
}

export default {
    login,
    register,
    getAllUser,
    deleteUser
}